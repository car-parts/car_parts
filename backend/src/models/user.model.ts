import {Entity, model, property} from '@loopback/repository';

@model()
export class User extends Entity {
  @property({
    id: true,
    type: 'string',
    required: false,
    mongodb: {dataType: 'ObjectId'},
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  username: string;

  @property({
    type: 'string',
    required: true,
    hidden: true,
  })
  password: string;

  @property({
    type: 'object',
    hidden: true,
  })
  accessToken?: {
    id: string;
    created: Date;
  };

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
