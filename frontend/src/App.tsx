import {ref, defineComponent, defineAsyncComponent, Ref, provide} from 'vue';
import {RouterView, useRouter} from 'vue-router';
import i18n from 'i18next';
import Menu from 'ant-design-vue/lib/menu';
import {UserOutlined} from '@ant-design/icons-vue';
import langEn from './i18n/en.json';
import './App.scss';

import {UserService, keyUserService, User} from './services/user-service';

enum MenuKeys {
  home = 'home',
  about = 'about',
  login = 'login',
  account = 'account',
  logout = 'logout',
}

const AppComponent = defineComponent({
  setup() {
    const userService = new UserService();
    provide(keyUserService, userService);

    const selectedMenu: Ref<string[]> = ref([MenuKeys.home]);
    const router = useRouter();
    const onMenuClick = (keys: string[]) => {
      if (keys[0] === MenuKeys.logout) {
        // TODO logout
      } else {
        router.push({path: keys[0]});
        selectedMenu.value = keys;
      }
    };

    const user: Ref<User | null> = ref(null);
    userService.onUserChange.subscribe((value) => {
      user.value = value;
      if (value) {
        router.push('home');
        // TODO should be updated automatically when route changes
        selectedMenu.value = [MenuKeys.home];
      }
    });

    return () => (
      <>
        <Menu
          mode="horizontal"
          selectedKeys={selectedMenu.value}
          onSelectChange={onMenuClick}
          theme="dark"
        >
          <Menu.Item key={MenuKeys.home}>{i18n.t('nav.home')}</Menu.Item>
          <Menu.Item key={MenuKeys.about}>{i18n.t('nav.about')}</Menu.Item>

          {user.value ? (
            <Menu.SubMenu
              key={MenuKeys.account}
              title={
                <span class="submenu-title-wrapper">
                  {user.value.username}
                  <UserOutlined
                    style={{
                      fontSize: '22px',
                      marginLeft: '8px',
                      verticalAlign: 'middle',
                    }}
                  />
                </span>
              }
            >
              <Menu.Item key={MenuKeys.logout}>{i18n.t('nav.logout')}</Menu.Item>
            </Menu.SubMenu>
          ) : (
            <Menu.Item key={MenuKeys.login}>{i18n.t('nav.login')}</Menu.Item>
          )}
        </Menu>
        <div id="content">
          <RouterView />
        </div>
      </>
    );
  },
});

export default defineComponent({
  name: 'App',
  setup() {
    const AsyncAppComponent = defineAsyncComponent({
      loader: async () => {
        await i18n.init({
          lng: 'en',
          resources: {
            en: {translation: langEn},
          },
        });
        // Uncomment to simulate loading delay
        // await new Promise((resolve) => setTimeout(resolve, 2000));
        return AppComponent;
      },
      // TODO better loading page
      loadingComponent: () => <div>Loading...</div>,
    });
    return () => (
      <>
        <AsyncAppComponent />
      </>
    );
  },
});
