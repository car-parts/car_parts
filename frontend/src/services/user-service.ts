import {InjectionKey} from 'vue';
import {BehaviorSubject, Observable} from 'rxjs';
import {apiRoot} from './http';

export const keyUserService: InjectionKey<UserService> = Symbol('userService');

export interface User {
  id: string;
  username: string;
}

export class UserService {
  private _authenticationToken?: string;
  private _user = new BehaviorSubject<User | null>(null);

  async login(param: {username: string; password: string}): Promise<{user: User, token: string}> {
    const response = await apiRoot.post('/users/login', param);

    this._authenticationToken = response.data.token;
    this._user.next(response.data.user);
    return response.data;
  }

  get authenticationToken(): string | undefined {
    return this._authenticationToken;
  }

  get user(): User | null {
    return this._user.value;
  }

  get onUserChange(): Observable<User | null> {
    return this._user;
  }
}
