import {defineComponent, inject, reactive, ref} from 'vue';
import Form from 'ant-design-vue/lib/form';
import Input from 'ant-design-vue/lib/input';
import Button from 'ant-design-vue/lib/button';
import {useForm} from '@ant-design-vue/use';
import i18n from 'i18next';
import './Login.scss';

import {keyUserService} from '../services/user-service';

interface LoginForm {
  username: string;
  password: string;
}

const usernameRegex = new RegExp('^(?!\\d)\\w+$');

export default defineComponent({
  name: 'Login',
  setup() {
    const userService = inject(keyUserService);

    const loginForm: LoginForm = reactive({username: '', password: ''});
    const rules = reactive({
      username: [
        {
          required: true,
          type: 'string',
          validator: async (rule: any, value: string) => {
            value = value && value.trim();
            if (!value) {
              throw [i18n.t('login.errors.usernameRequired')];
            }
            if (!usernameRegex.test(value)) {
              throw [i18n.t('login.errors.usernameInvalid')];
            }
          },
        },
      ],
      password: [
        {
          required: true,
          type: 'string',
          validator: async (rule: any, value: string) => {
            if (!value) {
              throw [i18n.t('login.errors.passwordRequired')];
            }
          },
        },
      ],
    });

    const {validate, validateInfos} = useForm(loginForm, rules);
    const login = async () => {
      try {
        await validate();
      } catch (err) {
        return;
      }

      try {
        const result = await userService?.login(loginForm);
        console.log(result);
      } catch (err) {
        console.log('Could not login:', err);
      }
    };

    const formItemLayout = {
      labelCol: {span: 4},
      wrapperCol: {offset: 2, span: 16},
    };

    return () => (
      <>
        <div id="login">
          <Form layout="horizontal" v-model={loginForm} validateTrigger="blur">
            <Form.Item
              label={i18n.t('login.username')}
              {...validateInfos.username}
              {...formItemLayout}
            >
              <Input v-model={[loginForm.username, 'value']} />
            </Form.Item>
            <Form.Item
              label={i18n.t('login.password')}
              {...validateInfos.password}
              {...formItemLayout}
            >
              <Input v-model={[loginForm.password, 'value']} />
            </Form.Item>
            <Form.Item wrapperCol={{offset: 8, span: 12}}>
              <Button type="primary" onClick={login}>
                Login
              </Button>
            </Form.Item>
          </Form>
        </div>
      </>
    );
  },
});
